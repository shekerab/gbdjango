from django.shortcuts import render
import json
import os.path
from django.http import HttpResponse

def index(request):
    context = {
        'title': 'Главная страница'
    }
    return render(request, 'mainapp/index.html', context)

def catalog(request):
    products = {}
    with open(os.path.join('mainapp', 'content.json')) as f:
        file_content = f.read()
        products = json.loads(file_content)
    context = {
        'title': 'Каталог товаров',
        'products': products
    }
    return render(request, 'mainapp/catalog.html', context)

def contacts(request):
    context = {
        'title': 'Контакты'
    }
    return render(request, 'mainapp/contacts.html', context)

def product(request, id):
    products = {}
    with open(os.path.join('mainapp', 'content.json')) as f:
        file_content = f.read()
        products = json.loads(file_content)

    id = str(id)
    if products and (id in products):
        product = products[id]
        context = {
            'title': 'Подробно о товаре '+product['name'],
            'product': product
        }
        return render(request, 'mainapp/product.html', context)
    else:
        return HttpResponse('Страница не найдена!!')

# Create your views here .
