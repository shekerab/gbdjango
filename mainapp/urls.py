from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='main'),
    path('catalog/', views.catalog, name='catalog'),
    path('contacts/', views.contacts, name='contacts'),
    path('catalog/<int:id>/', views.product, name='product'),
]
